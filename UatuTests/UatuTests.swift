//
//  UatuTests.swift
//  UatuTests
//

import XCTest
@testable import Uatu

class UatuTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /**
     Uatu does not support actual mapping or flatmapping (at this time) so to map
     or flatmap, we would set up 2 watchables and change the value of one from
     within the onNext handler of the other.
     */
    func testMapping() {
        
        struct Person {
            let name: String
            let favoriteColor: UIColor
        }
        
        let exp = expectation(description: "Wait for completion.")
        let peopleWatcher = Watchable(Person(name: "Mike", favoriteColor: .cyan))
        let nameWatcher = Watchable("")
        var disposeBag = DisposeBag()
        let person0 = Person(name: "Bob", favoriteColor: .red)
        let person1 = Person(name: "Tim", favoriteColor: .black)
        let person2 = Person(name: "John", favoriteColor: .blue)
        let person3 = Person(name: "Jim", favoriteColor: .brown)
        let people = [person0, person1, person2, person3]
        var onNextCalled = 0
        var peopleIndex = 0
        
        let expectedNamesList = [person0.name, person1.name, person2.name, person3.name]
        var namesList = [String]()
        
        peopleWatcher.value = people[peopleIndex]
        
        peopleWatcher.watch(onNext: { (person) in
            XCTAssert(person.name == people[peopleIndex].name, "\"\(person.name)\" should be \"\(people[peopleIndex].name)\"")
            XCTAssert(person.favoriteColor == people[peopleIndex].favoriteColor, "\(person.favoriteColor) should be \(people[peopleIndex].favoriteColor)")
            peopleIndex += 1
            onNextCalled += 1
            nameWatcher.onNext(person.name)
        }).disposed(by: disposeBag)
        
        nameWatcher.watch(onNext: { name in
            guard !name.isEmpty else { return }
            namesList.append(name)
        }) { exp.fulfill() }.disposed(by: disposeBag)
        
        peopleWatcher.value = people[1]
        peopleWatcher.value = people[2]
        peopleWatcher.value = people[3]
        
        disposeBag = DisposeBag()
        waitForExpectations(timeout: 3.0) { error in
            
            if let _ = error {
                XCTAssert(false, "Timeout while waiting for completion")
            }
            
            XCTAssert(onNextCalled == 4, "onNextCalled should be 4, not \(onNextCalled)")
            XCTAssert(expectedNamesList == namesList, "namesList not as expected.")
        }
    }
    
    func testAnEasyExample() {
        let exp = expectation(description: "Wait for completion.")
        let stringer = Watchable<String>("")
        var disposeBag = DisposeBag()
        let strings = ["apple", "bobby", "careless", "driving", "everywhere"]
        var onNextCalled = 0
        var onCompleteCalled = 0
        var onErrorCalled = 0
        var onDisposeCalled = 0
        var stringerIndex = 0
        
        stringer.value = strings[stringerIndex]
        
        stringer.watch(onNext: { (string) in
            XCTAssert(string == strings[stringerIndex], "\"\(string)\" should equal \"\(strings[stringerIndex])\"")
            stringerIndex += 1
            onNextCalled += 1
        }, onError: { error in
            onErrorCalled += 1
        }, onComplete: {
            onCompleteCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
            exp.fulfill()
        }).disposed(by: disposeBag)
        
        stringer.value = strings[1]
        stringer.value = strings[2]
        stringer.value = strings[3]
        stringer.value = strings[4]
        
        disposeBag = DisposeBag()
        waitForExpectations(timeout: 3.0) { error in
            
            if let _ = error {
                XCTAssert(false, "Timeout while waiting for completion")
            }
            
            XCTAssert(onNextCalled == 5, "onNextCalled should be 5, not \(onNextCalled)")
            XCTAssert(onCompleteCalled == 0, "onCompleteCalled should be 0, not \(onCompleteCalled)")
            XCTAssert(onErrorCalled == 0, "onErrorCalled should be 0, not \(onErrorCalled)")
            XCTAssert(onDisposeCalled == 1, "onDisposeCalled should be 1, not \(onDisposeCalled)")
        }
    }
    
    func testSequenceOfValues() {
        
        let exp = expectation(description: "Wait for completion.")
        let watchable = Watchable<String>("")
        let values = ["one", "red", "up", "car", "frank", "nice"]
        let wrapper = WatchableTestWrapper(watchable: watchable)
        let disposeBag = DisposeBag()
        var word = ""
        
        var completeCalled = 0
        var onNextCalled = 0
        var onErrorCalled = 0
        var onDisposeCalled = 0
        
        let completeShouldBeCalled = 1
        let onNextShouldBeCalled = values.count + 1
        let onErrorShouldBeCalled = 0
        let onDisposeShouldBeCalled = 1
        
        watchable.watch(onNext: { newValue in
            word = newValue
            onNextCalled += 1
        }, onError: { error in
            onErrorCalled += 1
        }, onComplete: {
            completeCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
            exp.fulfill()
        }).disposed(by: disposeBag)
        
        wrapper.disposeBag = disposeBag
        wrapper.setValuesAndEnd(values: values)
        
        waitForExpectations(timeout: 3.0) { error in
            
            if let _ = error {
                XCTAssert(false, "Timeout while waiting for completion")
            }
            
            guard let lastWord = values.last else {
                XCTAssert(false, "missing final value in array")
                return
            }
            XCTAssert(word == lastWord,
                      "word should be \(lastWord), not \(word)")
            XCTAssert(onNextCalled == onNextShouldBeCalled,
                      "onNextCalled should be \(onNextShouldBeCalled), not \(onNextCalled)")
            XCTAssert(onErrorCalled == onErrorShouldBeCalled,
                      "onErrorCalled should be \(onErrorShouldBeCalled), not \(onErrorCalled)")
            XCTAssert(completeCalled == completeShouldBeCalled,
                      "onNextCalled should be \(completeShouldBeCalled), not \(completeCalled)")
            XCTAssert(onDisposeCalled == onDisposeShouldBeCalled,
                      "onDisposeCalled should be \(onDisposeShouldBeCalled), not \(onDisposeCalled)")
        }
    }
    
    func testEndWithError() {
        
        let exp = expectation(description: "Wait for completion.")
        let initialValue = 0
        let watchable = Watchable<Int>(initialValue)
        let wrapper = WatchableTestWrapper(watchable: watchable)
        let values: [Int] = []
        var finalValue: Int? = nil
        let disposeBag = DisposeBag()
        
        var completeCalled = 0
        var onNextCalled = 0
        var onErrorCalled = 0
        var onDisposeCalled = 0
        
        let completeShouldBeCalled = 1
        let onNextShouldBeCalled = values.count + 1
        let onErrorShouldBeCalled = 1
        let onDisposeShouldBeCalled = 1
        
        watchable.watch(onNext: { newValue in
            finalValue = newValue
            //print("*** newValue: \(newValue)")
            onNextCalled += 1
        }, onError: { error in
//            print("TestEndWithError watchable ERROR: \(error.localizedDescription)")
            onErrorCalled += 1
        }, onComplete: {
            //print("*** COMPLETED. finalValue is \(finalValue)")
            completeCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
            exp.fulfill()
        }).disposed(by: disposeBag)
        
        wrapper.setValuesAndError(values: values)
        
        waitForExpectations(timeout: 3.0) { error in
            
            if let _ = error {
                XCTAssert(false, "Timeout while waiting for completion")
            }
            
            XCTAssert(finalValue == initialValue, "finalValue should be be \(initialValue), not \(finalValue ?? -1)")
            XCTAssert(onNextCalled == onNextShouldBeCalled,
                      "onNextCalled should be \(onNextShouldBeCalled), not \(onNextCalled)")
            XCTAssert(onErrorCalled == onErrorShouldBeCalled,
                      "onErrorCalled should be \(onErrorShouldBeCalled), not \(onErrorCalled)")
            XCTAssert(completeCalled == completeShouldBeCalled,
                      "completeCalled should be \(completeShouldBeCalled), not \(completeCalled)")
            XCTAssert(onDisposeCalled == onDisposeShouldBeCalled,
                      "onDisposeCalled should be \(onDisposeShouldBeCalled), not \(onDisposeCalled)")
        }
    }
    
    func testThreads() {
        
        let queue = DispatchQueue(label: "queue", qos: .userInitiated, attributes: [])
        let queue1 = DispatchQueue(label: "queue1", qos: .userInitiated, attributes: [DispatchQueue.Attributes.concurrent])
        let queue2 = DispatchQueue(label: "queue2", qos: .userInitiated, attributes: [DispatchQueue.Attributes.concurrent])
        
        let exp = expectation(description: "Wait for completion.")
        let initialValue = 0
        let watchable = Watchable<Int>(initialValue)
        let wrapper = WatchableTestWrapper(watchable: watchable)
        let values = [9101, 9102, 9103, 9104, 9105, 9106, 9107, 9108, 9109, 9110]
        var finalValue = -1
        let disposeBag = DisposeBag()
        
        var completeCalled = 0
        var onNextCalled = 0
        var onErrorCalled = 0
        var onDisposeCalled = 0
        
        let completeShouldBeCalled = 4
        let onNextShouldBeCalled = 102
        let onErrorShouldBeCalled = 4
        let onDisposeShouldBeCalled = 4
        let expectedFinalValue = values.last ?? -99999999999999999
        
        queue.async {
            watchable.watch(onNext: { newValue in
//                print("newValue \(newValue)")
                finalValue = newValue
                onNextCalled += 1
            }, onError: { error in
                print("TestThreads queue.async watchable ERROR: \(error.localizedDescription)")
                onErrorCalled += 1
            }, onComplete: {
                completeCalled += 1
            }, onDispose: {
                onDisposeCalled += 1
                exp.fulfill()
            }).disposed(by: disposeBag)
            watchable.value = 666
            watchable.value = 667
            watchable.value = 668
            watchable.value = 669
            watchable.value = 670
       }

        watchable.watch(onNext: { newValue in
            finalValue = newValue
            onNextCalled += 1
        }, onError: { error in
            print("TestThreads watchable ERROR: \(error.localizedDescription)")
            onErrorCalled += 1
        }, onComplete: {
            completeCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
//            exp.fulfill()
        }).disposed(by: disposeBag)
        
        queue1.async {
            watchable.watch(onNext: { newValue in
                finalValue = newValue
                onNextCalled += 1
            }, onError: { error in
                print("TestThreads queue1.async watchable ERROR: \(error.localizedDescription)")
                onErrorCalled += 1
            }, onComplete: {
                completeCalled += 1
            }, onDispose: {
                onDisposeCalled += 1
            }).disposed(by: disposeBag)
        }

        queue2.sync {

            watchable.watch(onNext: { newValue in
                finalValue = newValue
                onNextCalled += 1
            }, onError: { error in
                print("TestThreads queue2.sync watchable ERROR: \(error.localizedDescription)")
                onErrorCalled += 1
            }, onComplete: {
                completeCalled += 1
            }, onDispose: {
                onDisposeCalled += 1
            }).disposed(by: disposeBag)
            //wrapper.setValuesAndError(values: values)
        }
        wrapper.setValuesAndError(values: values)
        
        waitForExpectations(timeout: 6.0) { error in
            
            if let _ = error {
                XCTAssert(false, "Timeout while waiting for completion")
            }
            
            XCTAssert(finalValue == expectedFinalValue, "finalValue should be be \(expectedFinalValue), not \(finalValue)")
//            XCTAssert(onNextCalled == onNextShouldBeCalled,
//                      "onNextCalled should be \(onNextShouldBeCalled), not \(onNextCalled)")
            XCTAssert(onErrorCalled == onErrorShouldBeCalled,
                      "onErrorCalled should be \(onErrorShouldBeCalled), not \(onErrorCalled)")
            XCTAssert(completeCalled == completeShouldBeCalled,
                      "completeCalled should be \(completeShouldBeCalled), not \(completeCalled)")
            XCTAssert(onDisposeCalled == onDisposeShouldBeCalled,
                      "onDisposeCalled should be \(onDisposeShouldBeCalled), not \(onDisposeCalled)")
        }
    }
    
    
    func testThreadsWithThreadsInWrapper() {
        
        let queue = DispatchQueue(label: "queue", qos: .userInitiated, attributes: [])
        let queue1 = DispatchQueue(label: "queue1", qos: .userInitiated, attributes: [DispatchQueue.Attributes.concurrent])
        let queue2 = DispatchQueue(label: "queue2", qos: .userInitiated, attributes: [DispatchQueue.Attributes.concurrent])
        
        let exp = expectation(description: "Wait for completion.")
        let initialValue = 0
        let watchable = Watchable<Int>(initialValue)
        let wrapper = WatchableTestWrapper(watchable: watchable, useThreads: true)
        let values = [9101, 9102, 9103, 9104, 9105, 9106, 9107, 9108, 9109, 9110]
        let queueValues = [101, 102, 103, 104, 105, 106, 107, 108, 109, 110]
        var finalValue = -1
        let disposeBag = DisposeBag()
        
        var completeCalled = 0
        var onNextCalled = 0
        var onErrorCalled = 0
        var onDisposeCalled = 0
        
        let completeShouldBeCalled = 4
        let onNextShouldBeCalled = 102
        let onErrorShouldBeCalled = 0
        let onDisposeShouldBeCalled = 4
        let expectedFinalValue = values.last ?? -99999999999999999
        
        var itemsDisposed = 0
        func itemWasDisposed(_ name: String) {
            itemsDisposed += 1
//            print("items Disposed \(itemsDisposed) \(name)")
            if itemsDisposed == 4 {
                exp.fulfill()
            }
        }
        
        queue.async {
            watchable.watch(onNext: { newValue in
//                print("next queue.sync  \(newValue)")
                finalValue = newValue
                onNextCalled += 1
            }, onError: { error in
                onErrorCalled += 1
            }, onComplete: {
//                print("complete queue.async")
                completeCalled += 1
            }, onDispose: {
                onDisposeCalled += 1
                itemWasDisposed("queue.async")
            }).disposed(by: disposeBag)
            watchable.value = 666
            watchable.value = 667
            watchable.value = 668
            watchable.value = 669
            watchable.value = 670
            
           // wrapper.setValuesAndEnd(values: queueValues)
        }
        
        watchable.watch(onNext: { newValue in
//           print("next watch \(newValue)")
            //finalValue = newValue
            onNextCalled += 1
        }, onError: { error in
            onErrorCalled += 1
        }, onComplete: {
//            print("complete watch")
            completeCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
            itemWasDisposed("watchable")
        }).disposed(by: disposeBag)
        
        queue1.async {
            watchable.watch(onNext: { newValue in
//                print("next queue1.async \(newValue)")
                finalValue = newValue
                onNextCalled += 1
            }, onError: { error in
                onErrorCalled += 1
            }, onComplete: {
//                print("complete queue1.async")
                completeCalled += 1
            }, onDispose: {
                onDisposeCalled += 1
                itemWasDisposed("queue1")
            }).disposed(by: disposeBag)
            //wrapper.setValuesAndContinue(values: queueValues)
        }
        
        queue2.sync {
            
            watchable.watch(onNext: { newValue in
//                print("next queue2.sync  \(newValue)")
                finalValue = newValue
                onNextCalled += 1
            }, onError: { error in
                onErrorCalled += 1
            }, onComplete: {
//                print("complete queue2.sync")
                completeCalled += 1
            }, onDispose: {
                onDisposeCalled += 1
                itemWasDisposed("queue2")
            }).disposed(by: disposeBag)
            //wrapper.setValuesAndContinue(values: values)
        }
        wrapper.setValuesAndEnd(values: values)
        
        waitForExpectations(timeout: 4.0) { error in
            
            if let _ = error {
                XCTAssert(false, "Timeout while waiting for completion")
            }
            
            //XCTAssert(finalValue == expectedFinalValue, "finalValue should be be \(expectedFinalValue), not \(finalValue)")
//            XCTAssert(onNextCalled == onNextShouldBeCalled,
//                      "onNextCalled should be \(onNextShouldBeCalled), not \(onNextCalled)")
            XCTAssert(onErrorCalled == onErrorShouldBeCalled,
                      "onErrorCalled should be \(onErrorShouldBeCalled), not \(onErrorCalled)")
            XCTAssert(completeCalled == completeShouldBeCalled,
                      "completeCalled should be \(completeShouldBeCalled), not \(completeCalled)")
            XCTAssert(onDisposeCalled == onDisposeShouldBeCalled,
                      "onDisposeCalled should be \(onDisposeShouldBeCalled), not \(onDisposeCalled)")
        }
    }
    
    func testAutoDisposable() {
        
        let firstWord = "Initial"
        let exp = expectation(description: "Wait for completion.")
        let watchable = Watchable<String>(firstWord)
        let values = ["Ending"]
        var word = ""
        
        var completeCalled = 0
        var onNextCalled = 0
        var onErrorCalled = 0
        var onDisposeCalled = 0
        
        let completeShouldBeCalled = 0
        let onNextShouldBeCalled = 1
        let onErrorShouldBeCalled = 0
        let onDisposeShouldBeCalled = 1
        
        // because we are not setting a reference to the disposable,
        // it will dispose immediately, onNext should not be called.
        let _ = watchable.watch(onNext: { newValue in
            word = newValue
            onNextCalled += 1
        }, onError: { error in
            onErrorCalled += 1
        }, onComplete: {
            completeCalled += 1
        }) {
            onDisposeCalled += 1
            exp.fulfill()
        }
        
        watchable.value = values[0]

        waitForExpectations(timeout: 3.0) { error in
            
            if let _ = error {
                XCTAssert(false, "Timeout while waiting for completion")
            }

            XCTAssert(word == firstWord, "word should be be \(firstWord), not \(word)")
            XCTAssert(onNextCalled == onNextShouldBeCalled,
                      "onNextCalled should be \(onNextShouldBeCalled), not \(onNextCalled)")
            XCTAssert(onErrorCalled == onErrorShouldBeCalled,
                      "onErrorCalled should be \(onErrorShouldBeCalled), not \(onErrorCalled)")
            XCTAssert(completeCalled == completeShouldBeCalled,
                      "completeCalled should be \(completeShouldBeCalled), not \(completeCalled)")
            XCTAssert(onDisposeCalled == onDisposeShouldBeCalled,
                      "onDisposeCalled should be \(onDisposeShouldBeCalled), not \(onDisposeCalled)")
        }
    }
    
    func testDiposeBagCounts() {
        
        let exp = expectation(description: "Wait for completion.")
        
        let initialValue = 0
        let watchable = Watchable<Int>(initialValue)
        let initialValue1 = "One"
        let watchable1 = Watchable<String>(initialValue1)
        let initialValue2 = UIColor.red
        let watchable2 = Watchable<UIColor>(initialValue2)
        
        let wrapper = WatchableTestWrapper(watchable: watchable)
        let values: [Int] = [1, 2, 3,4, 5, 6, 7, 8, 9, 0, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        var disposeBag = DisposeBag()
        
        var completeCalled = 0
        var onNextCalled = 0
        var onErrorCalled = 0
        var onDisposeCalled = 0
        
        let completeShouldBeCalled = 1
        let onNextShouldBeCalled = 23
        let onErrorShouldBeCalled = 1
        let onDisposeShouldBeCalled = 3
        
        let watcher = watchable.watch(onNext: { newValue in
            onNextCalled += 1
        }, onError: { error in
//            print("TestDisposeBagCounts watcher ERROR: \(error.localizedDescription)")
            onErrorCalled += 1
        }, onComplete: {
            completeCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
            exp.fulfill()
        })
        watcher.disposed(by: disposeBag)
        
        let watcher1 = watchable1.watch(onNext: { newValue in
            onNextCalled += 1
        }, onError: { error in
//            print("TestDisposeBagCounts watcher1 ERROR: \(error.localizedDescription)")
            onErrorCalled += 1
        }, onComplete: {
            completeCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
        })
        
        watcher1.disposed(by: disposeBag)
        
        let watcher2 = watchable2.watch(onNext: { newValue in
            onNextCalled += 1
        }, onError: { error in
//            print("TestDisposeBagCounts watcher2 ERROR: \(error.localizedDescription)")
            onErrorCalled += 1
        }, onComplete: {
            completeCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
        })
        watcher2.disposed(by: disposeBag)
        
//        print("disposeBag.count \(disposeBag.count)")
        XCTAssert(disposeBag.count == 3, "Should be 3 items in disposeBag, not \(disposeBag.count)")
        watcher1.dispose()
//        print("disposeBag.count \(disposeBag.count)")
        XCTAssert(disposeBag.count == 2, "Should be 2 items in disposeBag, not \(disposeBag.count)")
        watcher2.dispose()
        XCTAssert(disposeBag.count == 1, "Should be 1 items in disposeBag, not \(disposeBag.count)")
        
        wrapper.setValuesAndError(values: values)
        
        waitForExpectations(timeout: 3.0) { error in

            if let _ = error {
                XCTAssert(false, "Timeout while waiting for completion")
            }
            XCTAssert(onNextCalled == onNextShouldBeCalled,
                      "onNextCalled should be \(onNextShouldBeCalled), not \(onNextCalled)")
            XCTAssert(onErrorCalled == onErrorShouldBeCalled,
                      "onErrorCalled should be \(onErrorShouldBeCalled), not \(onErrorCalled)")
            XCTAssert(completeCalled == completeShouldBeCalled,
                      "completeCalled should be \(completeShouldBeCalled), not \(completeCalled)")
            XCTAssert(onDisposeCalled == onDisposeShouldBeCalled,
                      "onDisposeCalled should be \(onDisposeShouldBeCalled), not \(onDisposeCalled)")
        }
    }
    
    
    func testDiposingEndsOnNextCalls() {
        
        let exp = expectation(description: "Wait for completion.")
        
        let initialValue = 0
        let watchable = Watchable<Int>(initialValue)
        
        var wrapper = WatchableTestWrapper(watchable: watchable)
        let values: [Int] = [1, 2, 3,4, 5, 6, 16, 17, 18, 19, 20]
        let finalValue = -1
        
        var disposeBag = DisposeBag()
        
        var completeCalled = 0
        var onNextCalled = 0
        var onErrorCalled = 0
        var onDisposeCalled = 0
        
        let completeShouldBeCalled = 3
        let onNextShouldBeCalled = (3 * (values.count + 1)) + (2 * (values.count))
        let onErrorShouldBeCalled = 0
        let onDisposeShouldBeCalled = 3
        
        watchable.watch(onNext: { newValue in
            onNextCalled += 1
//            print("a \(newValue)")
        }, onError: { error in
            onErrorCalled += 1
        }, onComplete: {
            completeCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
            setupNextWatcher()
        }).disposed(by: disposeBag)
        
        watchable.watch(onNext: { newValue in
            onNextCalled += 1
//            print("b \(newValue)")
        }, onError: { error in
            onErrorCalled += 1
        }, onComplete: {
            completeCalled += 1
        }, onDispose: {
            onDisposeCalled += 1
        }).disposed(by: disposeBag)
        wrapper.setValuesAndContinue(values: values)
        wrapper.setValuesAndEnd(values: values)
        
        waitForExpectations(timeout: 6.0) { error in
            
            if let _ = error {
                XCTAssert(false, "Timeout while waiting for completion")
            }
            XCTAssert(onNextCalled == onNextShouldBeCalled,
                      "onNextCalled should be \(onNextShouldBeCalled), not \(onNextCalled)")
            XCTAssert(onErrorCalled == onErrorShouldBeCalled,
                      "onErrorCalled should be \(onErrorShouldBeCalled), not \(onErrorCalled)")
            XCTAssert(completeCalled == completeShouldBeCalled,
                      "completeCalled should be \(completeShouldBeCalled), not \(completeCalled)")
            XCTAssert(onDisposeCalled == onDisposeShouldBeCalled,
                      "onDisposeCalled should be \(onDisposeShouldBeCalled), not \(onDisposeCalled)")
        }
        
        
        func setupNextWatcher() {
            
            let wrapper1 = WatchableTestWrapper(watchable: watchable)
            wrapper1.setValuesAndEnd(values: values)
            
            watchable.watch(onNext: { newValue in
                onNextCalled += 1
//                print("c \(newValue)")
            }, onError: nil,
               onComplete:  {
                completeCalled += 1
            }, onDispose: {
                onDisposeCalled += 1
                exp.fulfill()
            }).disposed(by: disposeBag)
        }
    }
    
}
