//
//  WeakRef.swift
//  Uatu
//

import Foundation

/**
 Wrapper class to wrap a weak reference.
 
 Originally found in Marco Santa's "Swift Arrays Holding Elements With Weak References"
 https://marcosantadev.com/swift-arrays-holding-elements-weak-references/
 */
class WeakRef<T> where T: AnyObject {
    
    private(set) weak var value: T?
    
    init(_ value: T?) {
        self.value = value
    }
}
